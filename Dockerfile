FROM docker.io/debian:buster-slim

# SSHD image for OpenShift Origin

ARG DO_UPGRADE=
ENV DEBIAN_FRONTEND=noninteractive \
    SSHD_PORT=2222

LABEL io.k8s.description="OpenSSH server." \
      io.k8s.display-name="OpenSSH server" \
      io.openshift.expose-services="2222:ssh" \
      io.openshift.tags="sshd,rsync" \
      io.openshift.non-scalable="true" \
      help="For more information visit https://gitlab.com/synacksynack/opsperator/docker-sshd" \
      maintainer="Samuel MARTIN MORO <faust64@gmail.com>" \
      version="7.9"

RUN set -x \
    && apt-get update \
    && mkdir -p /usr/share/man/man1 \
    && if test "$DO_UPGRADE"; then \
	echo "# Upgrade Base Image"; \
	apt-get -y upgrade; \
	apt-get -y dist-upgrade; \
    fi \
    && echo "# Install OpenSSH" \
    && apt-get -y install openssh-server rsync wget libnss-wrapper dumb-init \
    && mkdir -p /var/backup /var/run/sshd /run/sshd /home \
    && echo "# Fixing Permissions" \
    && chown -R 1001:root /etc/ssh /var/backup /run/sshd /var/run/sshd /home \
    && chmod -R g=u /var/backup /etc/ssh /var/run/sshd /home \
    && chmod 0700 /run/sshd \
    && echo "# Cleaning Up" \
    && apt-get remove --purge -y wget \
    && apt-get autoremove --purge -y \
    && apt-get clean \
    && sed -i -e "s|#PidFile.*$|PidFile /var/backup/tmp/sshd.pid|" \
	-e "s/#PasswordAuthentication .*$/PasswordAuthentication no/" \
	-e 's/#UsePrivilegeSeparation.*$/UsePrivilegeSeparation no/' \
	-e "s|#HostKey /etc/ssh/ssh_host_rsa_key.*|HostKey /var/backup/sshd/ssh_host_rsa_key|" \
	-e "s|#HostKey /etc/ssh/ssh_host_ecdsa_key.*|HostKey /var/backup/sshd/ssh_host_ecdsa_key|" \
	-e "s|#HostKey /etc/ssh/ssh_host_ed25519_key.*|HostKey /var/backup/sshd/ssh_host_ed25519_key|" \
	-e 's/StrictModes yes/StrictModes no/' \
	-e "s|UsePAM yes|UsePAM no|" \
	-e "s/#Port.*$/Port 2222/" /etc/ssh/sshd_config \
    && rm -rf /var/lib/apt/lists/* /usr/share/doc /usr/share/man

COPY config/* /
USER 1001
ENTRYPOINT ["dumb-init","--","/run-sshd.sh"]
