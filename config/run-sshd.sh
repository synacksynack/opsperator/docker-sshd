#!/bin/sh

if test "$DEBUG"; then
    set -x
fi

MY_HOME="${MY_HOME:-/home/backup}"
SSH_USERNAME=${SSH_USERNAME:-backup}

if test "`id -u`" -ne 0; then
    echo Setting up nsswrapper mapping `id -u` to $SSH_USERNAME
    pwentry="$SSH_USERNAME:x:`id -u`:`id -g`:$SSH_USERNAME:$MY_HOME:/bin/bash"
    ( grep -v ^$SSH_USERNAME: /etc/passwd ; echo "$pwentry" ) >/tmp/$SSH_USERNAME-passwd
    export NSS_WRAPPER_PASSWD=/tmp/$SSH_USERNAME-passwd
    export NSS_WRAPPER_GROUP=/etc/group
    export LD_PRELOAD=/usr/lib/libnss_wrapper.so
else
    chown root:root /run/sshd
fi
export HOME="$MY_HOME"

for d in "$HOME/.ssh" /var/backup/tmp /var/backup/sshd
do
    test -d "$d" || mkdir -p "$d"
done
chown -R $SSH_USERNAME:root "$HOME"
chmod -R g=u "$HOME"
chmod 0750 $HOME

ssh-keygen -q -N "" -t rsa -b 4096 -f /var/backup/sshd/ssh_host_rsa_key
ssh-keygen -q -N "" -t ecdsa -f /var/backup/sshd/ssh_host_ecdsa_key
ssh-keygen -q -N "" -t ed25519 -f /var/backup/sshd/ssh_host_ed25519_key
chown -R $SSH_USERNAME:root /var/backup/sshd
chmod 0600 /var/backup/sshd/*
chmod 0700 /var/backup/sshd /var/backup/tmp
if test -s /.ssh/id_rsa.pub; then
    cat /.ssh/id_rsa.pub >$HOME/.ssh/authorized_keys
    chmod 0600 $HOME/.ssh/authorized_keys
fi
chmod 0700 $HOME/.ssh
echo "" >$HOME/.ssh/authorized_keys2
chmod 0600 $HOME/.ssh/authorized_keys2

echo Starting SSHD
if test "$DEBUG"; then
    exec /usr/sbin/sshd -D -d -e -p "$SSHD_PORT"
else
    exec /usr/sbin/sshd -D -e -p "$SSHD_PORT"
fi
